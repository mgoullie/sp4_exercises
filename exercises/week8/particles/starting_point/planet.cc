#include "planet.hh"

void Planet::initself(std::istream &stream) {
	double x, y, z, vx, vy, vz, fx, fy, fz, _mass;
	std::string _name;
	sstr >> x >> y >> z >> vx >> vy >> vz >> fx >> fy >> fz >> _mass >> _name;
	position.push_back(x);
	position.push_back(y);
	position.push_back(z);
	velocity.push_back(vx);
	velocity.push_back(vy);
	velocity.push_back(vz);
	force.push_back(fx);
	force.push_back(fy);
	force.push_back(fz);
	mass = _mass;
	name = _name;
}

/* -------------------------------------------------------------------------- */

void Planet::printself(std::ostream &stream) const {
	double x, y, z, vx, vy, vz, fx, fy, fz, _mass;
	std::string _name;
	x = position[0];
	y = position[1];
	z = position[2];
	
	vx = velocity[0];
	vy = velocity[1];
	vz = velocity[2];

	fx = force[0];
	fy = force[1];
	fz = force[2];

	os << x << y << z << vx << vy << vz << fx << fy << fz << _mass << _name << "\n";
}
