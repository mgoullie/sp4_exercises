#include "csv_reader.hh"
#include "particles_factory_interface.hh"
#include <fstream>
#include <string>
#include <sstream>
/* -------------------------------------------------------------------------- */
CsvReader::CsvReader(const std::string& filename) : filename(filename) {}
/* -------------------------------------------------------------------------- */
void CsvReader::read(System& system) { this->compute(system); }
/* -------------------------------------------------------------------------- */

void CsvReader::compute(System& system) {
	ifstream is(filename);

	while (is.good()) {
		getline(is, line);
		stringstream ss(line);
		auto particle = ParticlesFactoryInterface::createParticle();	
		particle->initself(is);
		system.addParticle(particle);
	}
	

}

/* -------------------------------------------------------------------------- */
