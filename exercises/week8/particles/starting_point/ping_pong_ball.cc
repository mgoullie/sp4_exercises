#include "ping_pong_ball.hh"

/* -------------------------------------------------------------------------- */
void PingPongBall::printself(std::ostream& stream) const {
}

/* -------------------------------------------------------------------------- */

void PingPongBall::initself(std::istream& sstr) {
	double x, y, z, vx, vy, vz, fx, fy, fz, _mass >> _radius;
	sstr >> x >> y >> z >> vx >> vy >> vz >> fx >> fy >> fz >> _mass >> _radius;
	position.push_back(x);
	position.push_back(y);
	position.push_back(z);
	velocity.push_back(vx);
	velocity.push_back(vy);
	velocity.push_back(vz);
	force.push_back(fx);
	force.push_back(fy);
	force.push_back(fz);
	mass = _mass;
	radius = _radius;
}
