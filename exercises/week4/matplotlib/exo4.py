import matplotlib.pyplot as plt
import numpy as np
import math

fdata=np.loadtxt("data.plot")
#print(fdata)#array of vectors [[x y z][x2 y2 z2] ...]
print(fdata[0])#first row
print(fdata[0][0])#first row

n=len(fdata)
#for x in range(n):
#    print(fdata[x])
#
x_val=[fdata[i][0] for i in range(n)]
y_val=[fdata[i][1] for i in range(n)]
z_val=[fdata[i][2] for i in range(n)]
print(x_val)
plt.clf()
#plt.savefig("sinus.pdf")
plt.plot(x_val,y_val)
plt.plot(x_val,z_val)
plt.show()
plt.savefig("ana_mesured.pdf")
plt.clf()
plt.errorbar(fdata[:,0],fdata[:,1],np.sqrt((fdata[:,1]-fdata[:,2])**2))
plt.savefig("error_bar.pdf")

