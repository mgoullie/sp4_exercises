import matplotlib.pyplot as plt

#Ex 1
x = [0.,2.,4.,6.,8.,10.]
y = [0.,0.,0.,1.,1.,1. ]

x2 = [0.,3.,5.,7.,9.,11.]
y2 = [0.,0.,0.,1.,1.,1. ]

plt.plot(x,y,"bo",label='$f_1$')

#plt.savefig("dots.pdf")


#plt.clf()
#plt.plot(x,y, "rx--")
plt.plot(x2,y2,"rx--", label='$f_2$')
plt.title("My little plot")
plt.xlabel("x")
plt.ylabel("y")
#plt.savefig("crosses.pdf")
plt.legend()
plt.savefig("dots_and_crosses.pdf")

