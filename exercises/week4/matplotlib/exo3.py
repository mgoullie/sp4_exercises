import matplotlib.pyplot as plt
import numpy as np
import math

fdata=np.loadtxt("data.plot")

table=np.linspace(0.,1.,10)
sin2_x=[math.sin(x)**2 for x in table]
fig= plt.plot(table,sin2_x)
plt.savefig("sinus_square.pdf")

plt.clf()
sin_x=[math.sin(x) for x in table]
fig2= plt.plot(table,sin_x)
plt.savefig("sinus.pdf")
