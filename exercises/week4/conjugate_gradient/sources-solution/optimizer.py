#!/usr/bin/env python3

import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
from conjugate_gradient import solve as cgSolve
from scipy.optimize import minimize as scipySolve


def plotFunction(func, iterations, title):
    'Plotting function for plane and iterations'

    x = np.linspace(-3, 3, 100)
    y = np.linspace(-3, 3, 100)
    x, y = np.meshgrid(x, y)

    X = np.stack([x.flatten(), y.flatten()], axis=1)
    z = func(X).reshape((100, 100))

    xy = np.array(iterations)
    zs = func(xy)

    fig = plt.figure()
    ax = Axes3D(fig)
    ax.plot_surface(x, y, z, linewidth=0, antialiased=True,
                    cmap=plt.cm.viridis, alpha=0.2)
    ax.contour(x, y, z, 10, colors="k", linestyles="solid")
    ax.plot(xy[:, 0], xy[:, 1], zs, 'ro--')
    ax.grid(False)
    ax.view_init(elev=62., azim=143)
    ax.set_title(title)

    plt.xlabel('$x$')
    plt.ylabel('$y$')
    plt.show()

################################################################
# generic optimization program
################################################################


def optimize(method='cg', plot=False, func=None, **kwargs):

    if func is None:
        raise RuntimeError('Need a function to optimize')

    tol = 1e-6
    x0 = np.array([2, 2])

    if method == 'BFGS':
        optimizer = scipySolve
        if 'hess' in kwargs:
            del kwargs['hess']

    elif method == 'cg':
        optimizer = cgSolve

    else:
        raise RuntimeError('unknown solve method: ' + str(method))

    iterations = []
    iterations.append(x0)

    res = optimizer(
        func, x0, tol=tol, callback=lambda Xi: iterations.append(Xi),
        **kwargs)

    print('Method: ', method, ' Solution :', res)
    if plot is True:
        plotFunction(func, iterations, method)
