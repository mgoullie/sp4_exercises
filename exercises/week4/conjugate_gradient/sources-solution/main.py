#!/usr/bin/env python3

from optimizer import optimize
import numpy as np
import argparse

################################################################
# main function: parsing of program arguments
################################################################


if __name__ == '__main__':

    parser = argparse.ArgumentParser(
        description='Optimization exercise in python')

    parser.add_argument(
        'coeffs', type=float,
        nargs='+',
        help=('Specify the coefficients of the quadratic function,'
              ' in the following order:  a_11, a_12, a_21, a_22, b_1, b_2.'
              ' The coefficients are the terms of the matrix A'
              ' and the vector b in the quadratic form 1/2 x^T A x - x^T b.'))

    parser.add_argument(
        '-m', '--method', type=str,
        help=('Specify the method to use for the optimization.'
              ' Can be cg/BFGS'), default='cg')

    parser.add_argument(
        '-p', '--plot', type=bool,
        help=('Choose to plot or not the solution.'
              ' True/False'), default=False)

    args = parser.parse_args()
    A = np.array(args.coeffs[:4]).reshape(2, 2)
    b = np.array(args.coeffs[4:])

    # transform to dictionary
    args = vars(args)
    del args['coeffs']

    # definitions of the quadratic form
    def func(X, A, b):
        "functor calculation of the quadratic form"

        X = X.reshape(X.flatten().shape[0]//2, 2)

        # computes the quadratic form with einsum
        res = 0.5*np.einsum('ai,ij,aj->a', X, A, X)
        res -= np.einsum('i,ai->a', b, X)
        return res

    optimize(func=lambda x: func(x, A, b),
             jac=lambda x: A@x-b,
             hess=lambda x: A,
             **args)
