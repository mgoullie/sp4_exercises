class Series:
    def __init__(self):
        pass

    def compute(self, N):
        raise Exception("This is a virtual function")
    
    def getAnalyticPrediction(self):
        raise Exception("pure virtual function")


#question: what happens if there is no destructor?
