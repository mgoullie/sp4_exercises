from series import *
import numpy as np

class Pi(Series):
    def __init__(self):
        Series.__init__(self)

    def compute(self, N):
        series_value=0
        for i in range(1, N+1):
            series_value+=1/(i*i)
        series_value*=6.0
        series_value=np.sqrt(series_value)
        return series_value

