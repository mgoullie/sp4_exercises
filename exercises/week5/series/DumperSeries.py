from series import *

class DumperSeries:
    def __init__(self, series):
        self.series = series

    def dump(self):
        raise Exception("pure virtual function")

