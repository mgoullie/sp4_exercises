// Fixture class
class SystemTest : public ::testing::Test {
protected:
  void SetUp() override {
    // push 10 particles
    system.particles.resize(10);
  }

  void TearDown() override {
    // clear particles
    system.particles.clear();
  }
  System system;
};
