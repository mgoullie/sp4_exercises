#Import required modules
import sys

#Say hi, to be polite
#print("Hello World!")
#print the number of arguments povided to python
print("Number of arguments given to program: "+str(len(sys.argv)))
if len(sys.argv) > 1:
    message="Hello "+str(sys.argv[1])
    print(message)
else: 
    #Say hi, to be polite
    print("Hello World!")

#NB: put a warning when no argv[1]
#Type of sys.argv ? It is a list.
