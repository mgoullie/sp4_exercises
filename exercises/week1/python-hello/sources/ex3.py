#Import required modules
import sys

#Say hi, to be polite
#print("Hello World!")
#print the number of arguments povided to python
print("Number of arguments given to program: "+str(len(sys.argv)))
if len(sys.argv) > 1:
    n=int(sys.argv[1])
    for k in range(1, n+1):
        #print(k)
        k+=k
    print("Hello "+str(k))

else: 
    #Say hi, to be polite
    print("Hello User! Please provide an interger as argument.")
#n operations are needed to compute this series
