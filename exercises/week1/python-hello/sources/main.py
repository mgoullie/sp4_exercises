import sys
import series

print("Number of arguments given to program: "+str(len(sys.argv)))
if len(sys.argv) > 1:
    Niterations=int(sys.argv[1])
    #message="Hello "+str(sys.argv[1])
    #print(message)
    series.computeSeries(Niterations)
else: 
    #Say hi, to be polite
    print("Hello World! "+str(sys.argv[0]))
